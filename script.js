// dom elements
const domElements = {
    form: document.getElementById('form'),
    username: document.getElementById('username'),
    email: document.getElementById('email'),
    password: document.getElementById('password'),
    password2: document.getElementById('password2')
};


                                        //EVENT HANDLERS

//submit                                        
form.addEventListener('submit', function(event){
event.preventDefault();
validateUserInput([username, email, password, password2]);
validateInputLength(username, 3, 15);
validateEmail(email);
validateMatchingPasswords(password, password2)
});

function validateInputLength(element, minLength, maxLength){
    if(element.value.length > maxLength){
        showError(element, `${makeInitUppercase(element)} cannot exceed ${maxLength} characters`);
    }
    else if(element.value.length < minLength){
        showError(element, `${makeInitUppercase(element)} must be atlest ${minLength} characters`);
    }else{
        showSuccess(element)
    }
}



function validateUserInput(elements){
    elements.forEach(function(element){
        
        //check for empty string
        if(element.value.trim() === ''){
            showError(element, `${makeInitUppercase(element)} is required`);
        }
        else
            showSuccess(element);
    });
}

//show error
function showError(element, message){
    const formControl = element.parentElement;
    const small = formControl.querySelector('small');

    formControl.className = 'form-control error';
    small.innerText = message;
}

// validate email
function validateEmail(email){
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(re.test(email.value.trim())){
        showSuccess(email);
    }else{
        showError(email, "Must be a valid email address");
    }
}

function validateMatchingPasswords(password, confirmPassword){
    if(password.value != confirmPassword.value){
        showError(password2, 'Passwords do not match');
    }
}

//show success
function showSuccess(element){
    const formControl = element.parentElement;
    formControl.className = 'form-control success';
}

function makeInitUppercase(element){
    const firstInitial = element.id.charAt(0).toUpperCase();
    return firstInitial + element.id.slice(1);
}


